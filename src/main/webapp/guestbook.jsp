<%-- //[START all]--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
<head>
    <link type="text/css" rel="stylesheet" href="/stylesheets/main.css"/>
    <link type="text/css" rel="stylesheet" href="/stylesheets/custom.css"/>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="/js/jquery.simplePagination.js"></script>
    <link type="text/css" rel="stylesheet" href="/stylesheets/simplePagination.css"/>

        <script>
            $(document).ready(function() {
				$('[data-toggle="tooltip"]').tooltip();  
	
                $('#submit').click(function(event) {  
                    var inputPhone=$('#phone').val();

					var mob7 = /^[0-9]{3}[- ]{0,1}[0-9]{4}$/;
					var mob10 = /^[0-9]{3}[- ]{0,1}[0-9]{3}[- ]{0,1}[0-9]{4}$/;
					if (mob7.test(inputPhone) == false && mob10.test(inputPhone) == false) {
					    
					    validate(2);
					    $('#phone').focus();
					    return false;
					}
					else {
						validate(1);
					}

                 	$.get('guestbook',{phone:inputPhone},function(responseText) { 
						var splited = responseText.split("~");
                        $('#count').text("Total number of combinations: " + splited[0]); 
						
						showPageData(splited[1].split(","));
						
						/*var noOfPages = splited[0] / 100;
						//alert(noOfPages);
						var ul = $('#pagination');
						$("#pagination").empty();
						var i=0;
						for(i=1;i<=noOfPages;i++) {
						    $('<li><a href=\"javascript:showPage('+i+')\">'+i+'</a></li>',{text:i}).appendTo(ul);
						}*/
						
						
						$('#pages').pagination('disable');
						$('#pages').pagination('updateItems', splited[0]);
						$('#pages').pagination('updateItemsOnPage', 100);
						$('#pages').pagination('selectPage', 1);
						$('#pages').pagination('enable');
                    });
                });
            });

			$(function() {
			    $("#pages").pagination({
			        items: 100,
			        itemsOnPage: 100,
			        cssStyle: 'light-theme',
			        onPageClick: function(pageNumber) {
			            
			            var inputPhone=$('#phone').val();
			
			                 $.get('guestbook',{phone:inputPhone,page:pageNumber},function(responseText) { 
									var splited = responseText.split("~");
									
									showPageData(splited[1].split(","));
			                 });
			        }
			    });
			});
			
			function validate(level) {
				if (level == 0) {
					//form-group
				    $('#formVal').removeClass('has-error');
				    $('#formVal').removeClass('has-success');
				    
				    $('#iconVal').removeClass('glyphicon-ok');
				    $('#iconVal').removeClass('glyphicon-remove');
					$('#iconVal').css("display", "none");
				}
				else if (level == 1) {
				    //form-group has-success has-feedback
				    $('#formVal').removeClass('has-error');
				    $('#formVal').addClass('has-success');
				    
				    //glyphicon glyphicon-ok form-control-feedback		//	for ok
				    $('#iconVal').removeClass('glyphicon-remove');
				    $('#iconVal').addClass('glyphicon-ok');
				    $('#iconVal').css("display", "block");
					
				}
				else {
			    	//form-group has-error has-feedback
			    	$('#formVal').removeClass('has-success');
			    	$('#formVal').addClass('has-error');
			    	
			    	//glyphicon glyphicon-remove form-control-feedback	//	for error
				    $('#iconVal').removeClass('glyphicon-ok');
				    $('#iconVal').addClass('glyphicon-remove');
				    $('#iconVal').css("display", "block");
				}
			}
			
			function showPage(pageNumber) {
	            
	            var inputPhone=$('#phone').val();

                 $.get('guestbook',{phone:inputPhone,page:pageNumber},function(responseText) { 
						var splited = responseText.split("~");
						
						showPageData(splited[1].split(","));
                 });
	        }
			    
			
			function showPageData(pageData) {
				var ul = $('#ulCombinations');
				ul.empty();
				$.each(pageData,function(i,obj) {
				    $('<li>',{text:obj}).appendTo(ul);
				});
			}
			
	</script>
</head>

<body>
<%
	
    String phone = request.getParameter("phone");
    if (phone == null) {
    	phone = "2403865599";
    }
    pageContext.setAttribute("phone", phone);
    UserService userService = UserServiceFactory.getUserService();
    User user = userService.getCurrentUser();
    if (user != null) {
        pageContext.setAttribute("user", user);
%>
<div class="container-fluid">
 
<h3>Hello, ${fn:escapeXml(user.nickname)}! (You can
    <a href="<%= userService.createLogoutURL(request.getRequestURI()) %>">sign out</a>.)</h3>
    
    <form action="/guestbook" method="get" class="form-inline" role="form">
    
	    <div id="formVal" class="form-group has-success has-feedback">
		    <label for="phone">Enter a 7 or 10 digit phone number :</label>
		    <input type="text" name="phone" id="phone" class="form-control" value="${fn:escapeXml(phone)}"
		    data-toggle="tooltip" data-placement="bottom" title="Please enter a valid 7 or 10 digit number. eg: 240 386 6106, 3866106"/>
		    <span id="iconVal" class="glyphicon glyphicon-ok form-control-feedback"></span>
	    </div>
	    <div><input type="button" id="submit" class="btn btn-primary" value="Show Combinations"/></div>
	    
	    <div id="countDiv" class="container-fluid"><label id="count"></label></div>
	    
	    <div class="container">
		  <ul id="pagination" class="pagination pagination-sm">
		  </ul>
		</div>
	  	<div id="pages"></div>
   		<ul id="ulCombinations" style="list-style: none;"></ul>
    	
	</form>

<%
} else {
%>
<h3>Hello!
    <a href="<%= userService.createLoginURL(request.getRequestURI()) %>">Sign in</a>
    to include your name.</h3>
<%
    }
%>
</div>
</body>
</html>
<%-- //[END all]--%>
