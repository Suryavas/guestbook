package com.example.guestbook;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class KeyPadCombination {
	
	//	define phone key to alpha-numeric map
	static final HashMap<Character,String> mapKeypad = new HashMap<Character,String>(){{
		put(' '," ");
		put('0',"0");
		put('1',"1");
		put('2',"2ABC");
        put('3',"3DEF");
        put('4',"4GHI");
        put('5',"5JKL");
        put('6',"6MNO");
        put('7',"7PQRS");
        put('8',"8TUV");
        put('9',"9WXYZ");
    }} ;
    
    /**
     * This method is used to prepare all combinations for given number
     * @param digits as input number
     * @return all Combinations
     */
	public ArrayList<String> letterCombinations(String digits) {
        ArrayList<String> prevDigitCombinations = new ArrayList<String>();
        ArrayList<String> nThDigitCombinations = new ArrayList<String>();
        prevDigitCombinations.add("");	//	assume first combination is blank to start/enter into loop

        String currentDigitPossibleLetters = null;
        //	run the loop for each digit to make its combinations
        for(int i=0;i<digits.length();i++){
        	
        	//	run loop for previous digits combinations formed
            for(String str: prevDigitCombinations) {
            	currentDigitPossibleLetters = mapKeypad.get(digits.charAt(i));
                for(int j=0;j<currentDigitPossibleLetters.length();j++)
                    nThDigitCombinations.add(str+currentDigitPossibleLetters.charAt(j));
            }
            prevDigitCombinations = nThDigitCombinations;
            nThDigitCombinations = new ArrayList<String>();
        }      
        return prevDigitCombinations;
    }
	
	/**
	 * This method is used to give page wise combination based on given previous digit combinations, instead-of recalculating again 
	 * @param prevDigitCombinations previously calculated combinations except last digit
	 * @param lastDigit last digit to calculate combination
	 * @param pageNo
	 * @param pageSize
	 * @return all Combinations
	 */
	public ArrayList<String> letterCombinationsByPage(ArrayList<String> prevDigitCombinations, Character lastDigit, int pageNo, int pageSize) {
        ArrayList<String> nThDigitCombinations = new ArrayList<String>();

        String currentDigitPossibleLetters = null;
        currentDigitPossibleLetters = mapKeypad.get(lastDigit);
    	//int totalCount = prevDigitCombinations.size()*currentDigitPossibleLetters.length();	//27
		//int noOfPages = totalCount/pageSize;	//27/5=5.4
		
        //int currentPage = (pageSize*((pageNo-1)*currentDigitPossibleLetters.length()))/prevDigitCombinations.size();
        int currentPage = (pageSize*((pageNo-1)))/currentDigitPossibleLetters.length();
        int offSet = 0;
        if (currentPage > 0) {
        	offSet = (pageSize * (pageNo-1)) - (( currentDigitPossibleLetters.length()* currentPage));
        }
        //int offSet = ;
		//System.out.println("currentPage = " + currentPage);
		//System.out.println("currentPage = " + currentPage);
		//System.out.println("offSet = " + offSet);
//		for (currentPage =0; currentPage<prevDigitCombinations.size() && nThDigitCombinations.size() < pageSize; currentPage++){
//			if (currentPage*currentDigitPossibleLetters.length() < (pageSize*((pageNo-1)))) {
//				continue;
//			}
    	//	run loop for previous digits combinations formed
        for( ;
        		currentPage < prevDigitCombinations.size() && nThDigitCombinations.size() < pageSize; currentPage++) {
        	//System.out.println("currentPage = " + currentPage + "\toffSet = " + offSet);
            for(int j=0; j<currentDigitPossibleLetters.length() && nThDigitCombinations.size() < pageSize; j++) {
                nThDigitCombinations.add(prevDigitCombinations.get(currentPage)+currentDigitPossibleLetters.charAt(j));
            }
            //	clear offset, as it is required only for start position
            //offSet = 0;
        }
        return nThDigitCombinations;
    }
	
	public ArrayList<String> getPageCombinations(ArrayList<String> calculatedCombos, String input, int pageNo, int pageSize) {
		return letterCombinationsByPage(calculatedCombos, input.charAt(input.length()-1), pageNo, pageSize);
	}
	
	public int getTotalCombinations(ArrayList<String> calculatedCombos, String input) {
		return calculatedCombos.size()*mapKeypad.get(input.charAt(input.length()-1)).length();
	}
	
	public ArrayList<String> getLastButOneDigitCombinations(String input) {
		return letterCombinations(input.substring(0, input.length()-1));
	}
	
    public static void main(String[] args) {
    	String input = "2403865599";
    	int pageSize = 100;
    	KeyPadCombination combination = new KeyPadCombination();
		
    	//ArrayList<String> letterCombinations = combination.getLastButOneDigitCombinations(input);
    	
    	//System.out.println(input);
    	System.out.println(combination.letterCombinations(input).size());
		/*System.out.println("Total number of combinations: " + combination.getTotalCombinations(letterCombinations, input));
		ArrayList<String> pageData = combination.getPageCombinations(letterCombinations, input, 1, pageSize);
		System.out.println(pageData);
		System.out.println(combination.getPageCombinations(letterCombinations, input, 2, pageSize));
		System.out.println(combination.getPageCombinations(letterCombinations, input, 3, pageSize));
		System.out.println(combination.getPageCombinations(letterCombinations, input, 4, pageSize));
		System.out.println(combination.getPageCombinations(letterCombinations, input, 5, pageSize));
		System.out.println(combination.getPageCombinations(letterCombinations, input, 6, pageSize));
		//System.out.println(combination.letterCombinationsByPage(letterCombinations, "4", 3, 10));
*/    }
}
