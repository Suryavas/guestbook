/**
 * Copyright 2014 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
//[START all]
package com.example.guestbook;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GuestbookServlet extends HttpServlet {
	KeyPadCombination keyPadCombination;
	
	@Override
	public void init() {
		this.keyPadCombination = new KeyPadCombination();
	}
	
  @Override
  public void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws IOException {
    if (req.getParameter("testing") != null) {
      resp.setContentType("text/plain");
      resp.getWriter().println("Hello, this is a testing servlet. \n\n");
      Properties p = System.getProperties();
      p.list(resp.getWriter());

    } else {
      UserService userService = UserServiceFactory.getUserService();
      User currentUser = userService.getCurrentUser();

      if (currentUser != null) {
        resp.setContentType("text/plain");
        String input = req.getParameter("phone");
        int page = 1;
    	try {
			page = Integer.parseInt(req.getParameter("page"));
		} catch (NumberFormatException e) {
		}
    	int pageSize = 100;
    	
        ArrayList<String> letterCombinations = null;
        String lastInput = (String)req.getSession(true).getAttribute("lastInput");
        
        if (input.equals(lastInput)) {
        	letterCombinations = (ArrayList<String>)req.getSession(true).getAttribute("letterCombinations");
        }
        else {
        	letterCombinations = keyPadCombination.getLastButOneDigitCombinations(input);
        	System.out.println("LastButOneDigitCombinations calculated");
        }
        req.getSession(true).setAttribute("letterCombinations", letterCombinations);
        req.getSession(true).setAttribute("lastInput", input);
        
		//System.out.println("page = " + page);
    	//resp.getWriter().println("Total number of combinations: " + keyPadCombination.getTotalCombinations(letterCombinations, input));
        resp.getWriter().println(keyPadCombination.getTotalCombinations(letterCombinations, input));
    	resp.getWriter().println("~");
		ArrayList<String> pageData = keyPadCombination.getPageCombinations(letterCombinations, input, page, pageSize);
		String pageData2Str = pageData.toString();
		resp.getWriter().println(pageData2Str.substring(1, pageData2Str.length()-1));
		
      } else {
        resp.sendRedirect(userService.createLoginURL(req.getRequestURI()));
      }
    }
  }
}
//[END all]
